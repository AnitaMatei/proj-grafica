#pragma once
#include "Body.h"

class CircleBody : public Body
{
public:
	CircleBody();
	~CircleBody();


	CircleBody(Vector2d const &position, Vector2d const &velocity, Vector2d const &acceleration, double mass, int radius, DrawableEntity* parent, bool active = true);

	int getRadius();
	void setRadius(int radius);
	void hasCollided(int collidedWithId) override;

private:
	int radius;
};

