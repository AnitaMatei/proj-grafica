#pragma once
#include "Ball.h"


class Cue : 
	public DrawableEntity
{
public:
	Cue();
	Cue(Color color, Ball* cueBall);
	~Cue();


	void update(float deltaTime) override;
	void checkInput(int button, int state, int x, int y);
	void draw() override;
	void draw3D() override;
	void updateMousePos(int x, int y);


private:
	void create2DObject() override;
	void create3DObject() override;
	void physicalBodyHasCollided(int collidedWithId) override;

	Vector2d getMouseBallNormalised();
	Vector2d getMouseBall();


	static const int MAX_OFFSET = 100;
	static const double HIT_ANIMATION_LENGTH;
	bool released = false;
	Vector2d releasedPosition;
	Vector2d hitAnimationVector;
	Vector2d hitAnimationProgress;
	double hitAnimationTimer;

	Color color;
	Ball* cueBall;
	bool held = false;
	Vector2d mousePos;
	double hitStrength;
	vector<Vector2d> definingPoints;
	GLuint displayList2D;
	GLuint displayList3D;

};

