#pragma once
#include "Body.h"
class RectangleBody :
	public Body
{
public:
	RectangleBody();
	virtual ~RectangleBody();
	RectangleBody(Vector2d position, int width, int height, DrawableEntity* parent, bool active = true);


	void hasCollided(int collidedWithId) override;

	int getWidth();
	int getHeight();

private:
	int width;
	int height;
};

