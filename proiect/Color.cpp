#include "Color.h"


Color Color::Red = Color(1, 0, 0);
Color Color::Green = Color(0, 1, 0);
Color Color::Blue = Color(0, 0, 1);
Color Color::Pink = Color(1, .412, .706);
Color Color::Orange = Color(1, .549, 0);
Color Color::Gold = Color(1, .843, 0);
Color Color::Violet = Color(.549, .169, .886);
Color Color::Yellow = Color(1, 1, 0);
Color Color::Lime = Color(.196, .804, .196);
Color Color::SpringGreen = Color(0, 1, .498);
Color Color::Black = Color(0, 0, 0);
Color Color::White = Color(1, 1, 1);
Color Color::Gray = Color(.502, .502, .502);
Color Color::Aqua = Color(0, 1, 1);
Color Color::SandyBrown = Color(.957,.643,.376);
Color Color::Brown = Color(0.545, 0.271, 0.075);
Color Color::Sienna = Color(0.627, 0.322, 0.176);
Color Color::LawnGreen = Color(0.486, 0.988, 0.000);

Color::Color()
{
}

Color::Color(float r, float g, float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}


Color::~Color()
{
}
