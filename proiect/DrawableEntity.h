#pragma once

class DrawableEntity
{
public:
	DrawableEntity();
	~DrawableEntity();

	virtual void draw() = 0;
	virtual void draw3D() = 0;
	virtual void update(float deltaTime) = 0;
	virtual void physicalBodyHasCollided(int collidedWithId) = 0;
protected:
	virtual void create2DObject() = 0;
	virtual void create3DObject() = 0;
};

