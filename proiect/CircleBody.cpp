#include "CircleBody.h"



CircleBody::CircleBody()
{
}


CircleBody::~CircleBody()
{
}

CircleBody::CircleBody(Vector2d const & position, Vector2d const & velocity, Vector2d const & acceleration, double mass, int radius, DrawableEntity* parent, bool active):
	Body(position,velocity,acceleration,mass,Body::Circle, parent, active)
{
	this->radius = radius;
}

int CircleBody::getRadius()
{
	return radius;
}

void CircleBody::setRadius(int radius)
{
	this->radius = radius;
}

void CircleBody::hasCollided(int collidedWithId)
{
	parent->physicalBodyHasCollided(collidedWithId);
}
