#pragma once
#include "PhysicsEngine.h"
#include "Ball.h"
#include "Cue.h"
#include "Rules.h"
#include "Wall.h"
#include "Camera.h"
#include <chrono>
#include <fstream>
#include <string>

using namespace std;
using namespace chrono;


class Game
{
public:

	void start(Vector2d windowSize, int argc, char **argv);

	static Game& getInstance() {
		return instance;
	}

private:
	Game();

	static Game instance;

	void initWindow(Vector2d windowSize, int argc, char **argv);
	void createBalls();
	void createWalls();
	void resetCueBall();
	void drawUi();
	void drawTable();
	void switchRenderMode();
	seconds getDeltaTime(high_resolution_clock& lastFrame);
	void registerPlayer();
	void initializeGame();
	void endGame();
	void createMenus();
	static void createMainMenu(int id);
	static void createSubMenu(int id);


	static void updateGame(void);
	static void drawGame(void);
	static void drawGame2D(void);
	static void drawGame3D(void);
	static void prepareDrawGame3D();
	static void prepareDrawGame2D();
	static void checkMouseInput(int button, int state, int x, int y);
	static void checkKeyboardInput(unsigned char key, int x, int y);
	static void checkKeyboardInputUp(unsigned char key, int x, int y);
	static void checkPassiveMouseMotion(int x, int y);
	static void checkMouseMotion(int x, int y);
	static void shadowmatrix(float shadowMat[4][4], float groundplane[4], float lightpos[4]);

	string playerName;
	steady_clock::time_point startTime;
	int score;
	double offset;
	bool gameOver;

	PhysicsEngine engine;
	vector<Ball*> balls;
	vector<Wall*> walls;
	Ball* cueBall;
	Cue* cue;
	Camera* camera;


	Vector2d windowSize;
	bool render2D = false;
};

