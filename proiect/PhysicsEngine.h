#pragma once
#include "Vector2d.h"
#include "CircleBody.h"
#include "Rules.h"
#include <iostream>
#include <vector>


using namespace std;

class PhysicsEngine
{
public:
	PhysicsEngine();
	~PhysicsEngine();


	void addBody(Body *body);

	//updates the balls' positions, checks for collisions, etc.
	void updatePhysics(double deltaTime);
	void addToBodyRemovalQueue(Body *body);

private:
	void handleCollidingCircles(CircleBody &b1, CircleBody &b2);
	void emptyBodyRemovalQueue();
	bool isHole(CircleBody& b);

	//daca suma razelor celor 2 cercuri este mai mare decat distanta dintre centrul acestora, atunci cele 2 se intersecteaza
	bool doCirclesCollide(CircleBody& b1, CircleBody& b2);

	double getDistanceBetweenCircles(Body &b1, Body &b2);


	vector<Body*> bodies;
	vector<Body*> bodyRemovalQueue;
};

