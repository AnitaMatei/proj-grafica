#pragma once
#include "Vector2d.h"
#include "DrawableEntity.h"
#include <iostream>

using namespace std;

class Body
{
public:
	enum BodyType {
		Circle,
		Rectangle
	};

	Body();
	Body(Vector2d const &position, Vector2d const &velocity, Vector2d const &acceleration, double mass, BodyType bodyType, DrawableEntity* parent, bool active);
	virtual ~Body();

	virtual void hasCollided(int collidedWithId) = 0;

	Vector2d & getAcceleration();
	void setAcceleration(Vector2d const &acceleration);
	void setVelocity(Vector2d const &velocity);
	Vector2d& getVelocity();
	Vector2d& getPosition();
	void setPosition(Vector2d const & position);
	void addToAcceleration(Vector2d const &addition);
	void addToVelocity(Vector2d const &addition);
	void addToPosition(Vector2d const &addition);
	double getMass();
	BodyType getType();
	int getId();
	bool isActive();
	void activate(bool active);

	static int count;

protected:
	DrawableEntity* parent;

	Vector2d velocity;
	Vector2d acceleration;
	Vector2d position;

	double mass;
	BodyType bodyType;
	bool active;
	int id;
};

