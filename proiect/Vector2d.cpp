#include "Vector2d.h"



Vector2d::Vector2d(double x, double y) {
	this->x = x;
	this->y = y;
}


Vector2d::~Vector2d()
{
}

Vector2d Vector2d::multiplyBy(double factor)
{
	return Vector2d(x*factor,y*factor);
}

Vector2d Vector2d::normalise(Vector2d & a, Vector2d & b)
{
	double magnitude = sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
	Vector2d returnVal((a.x - b.x) / magnitude, (a.y - b.y) / magnitude);
	return returnVal;
}
