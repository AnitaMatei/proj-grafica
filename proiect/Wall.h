#pragma once
#include "DrawableEntity.h"
#include "PhysicsEngine.h"
#include <gl/freeglut.h>
#include "RectangleBody.h"
#include "Vector3d.h"
#include <math.h>

class Wall :
	public DrawableEntity
{
public:
	Wall(Vector2d position, int width, int height, PhysicsEngine &engine);
	virtual ~Wall();
	void update(float deltaTime) override {

	}
	void draw() override;
	void draw3D() override;
	void physicalBodyHasCollided(int collidedWithId) override {

	}
	Vector2d getWallCenter();
	
	RectangleBody& getBody();
	Color& getColor();

private:
	void create2DObject() override;
	void create3DObject() override;


	PhysicsEngine* engine;
	Color color;
	RectangleBody body;
	GLuint displayList2D;
	GLuint displayList3D;
	double rotation;
};

