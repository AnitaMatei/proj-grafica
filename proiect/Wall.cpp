#include "Wall.h"




Wall::Wall(Vector2d position, int width, int height, PhysicsEngine& engine) : body(position, width, height, this)
{
	engine.addBody(&this->body);
	this->engine = &engine;
	this->color = Color::Brown;

	this->create2DObject();
	this->create3DObject();
}

Wall::~Wall()
{
}

void Wall::draw()
{
	glPushMatrix();
	glTranslated(body.getPosition().x, body.getPosition().y, 0);
	glCallList(displayList2D);
	glPopMatrix();

}

void Wall::draw3D()
{
	glPushMatrix();

	float ambientCoeff[] = { .25,.15,.065,1.0 };
	float diffuseCoeff[] = { .4,.24,.104,1.0 };
	float specularCoeff[] = { .78,.24,.104,1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambientCoeff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseCoeff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularCoeff);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 1.0);

	Vector3d position = Vector3d::translate2DTo3D(body.getPosition());
	position.y = Rules::TABLE_HEIGHT_3D + 20;

	glTranslated(position.x, position.y, position.z);


	glCallList(displayList3D);
	glPopMatrix();

}

Vector2d Wall::getWallCenter()
{
	return body.getPosition();
}

RectangleBody& Wall::getBody()
{
	return body;
}

Color& Wall::getColor()
{

	return color;
}

void Wall::create2DObject()
{

	displayList2D = glGenLists(1);
	glNewList(displayList2D, GL_COMPILE);

	glColor3f(color.r, color.g, color.b);
	glRecti(-body.getWidth() / 2, -body.getHeight() / 2, body.getWidth() / 2, body.getHeight() / 2);

	glEndList();
}

void Wall::create3DObject()
{
	displayList3D = glGenLists(1);

	glNewList(displayList3D, GL_COMPILE);


	glBegin(GL_QUADS);
	// Top face CCW
	glColor3f(color.r, color.g, color.b);
	glVertex3f(-body.getWidth() / 2, 0, -body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, 0, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, 0, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, 0, -body.getHeight() / 2);


	// Bottom face
	glVertex3f(-body.getWidth() / 2, -20, -body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, -body.getHeight() / 2);

	// Front face
	glVertex3f(-body.getWidth() / 2, 0, body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, 0, body.getHeight() / 2);

	// Back face
	glVertex3f(-body.getWidth() / 2, 0, -body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, -20, -body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, -body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, 0, -body.getHeight() / 2);

	// Left face
	glVertex3f(-body.getWidth() / 2, 0, -body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, -20, -body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(-body.getWidth() / 2, 0, body.getHeight() / 2);

	// Right face
	glVertex3f(body.getWidth() / 2, 0, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, -20, -body.getHeight() / 2);
	glVertex3f(body.getWidth() / 2, 0, -body.getHeight() / 2);

	glEnd();

	glEndList();
}
