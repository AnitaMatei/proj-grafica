#pragma once
#include <gl/freeglut.h>
#include "CircleBody.h"
#include "PhysicsEngine.h"
#include "Vector3d.h"
#include "Color.h"
#include "DrawableEntity.h"
#include "Rules.h"

class Ball : public DrawableEntity
{
public:
	Ball();
	Ball(Color color,int radius, PhysicsEngine & engine, Vector2d  const& position, Vector2d  const& velocity, Vector2d const& acceleration, bool active=true);
	~Ball();


	void update(float deltaTime) override;
	void draw() override;
	void draw3D() override;
	void physicalBodyHasCollided(int collidedWithId) override;

	Vector2d projectPointOntoCircumference(Vector2d p);

	CircleBody& getBody();
	Color& getColor();
	bool isIntoHole();
	void setIntoHole(bool intoHole);

private:
	void create2DObject() override;
	void create3DObject() override;

	static const double TWO_PI;
	PhysicsEngine *engine;
	Color color;
	CircleBody body;
	GLuint displayList2D;
	GLuint displayList3D;

	bool intoHole = false;
};

