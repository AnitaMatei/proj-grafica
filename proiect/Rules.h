#pragma once
#include <vector>
#include "Color.h"
#include "Vector2d.h"

using namespace std;

class Rules
{
public:
	static vector<Vector2d> HOLES_POSITIONS;
	static vector<int> HOLES_IDS;

	static Vector2d CUE_BALL_START_POSITION;
	static int CUE_BALL_ID;

	static vector<Vector2d> PLAY_BALLS_START_POSITIONS;
	static vector<int> PLAY_BALLS_IDS;
	static vector<Color> PLAY_BALLS_COLORS;

	static double TABLE_HEIGHT_3D;

private:
	Rules();
	~Rules();
};

