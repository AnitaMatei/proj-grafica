#pragma once
#include "DrawableEntity.h"
#include "Vector3d.h"
#include <iostream>
#include <gl/freeglut.h>
#include <vector>


class Camera
{
public:
	Camera();


	void update(float deltaTime);

	void checkMouseInput(int button, int state, int x, int y);
	void checkInput(unsigned char key, int x, int y);
	void checkInputUp(unsigned char key, int x, int y);
	void updateMousePos(int x, int y);

	const Vector3d& getPosition();
	const Vector3d& getLos();

	~Camera();

private:
	enum {
	FORWARD = 'w',
	BACKWARDS = 's',
	LEFT = 'a',
	RIGHT = 'd',
	STOP_CAMERA = 'q'
	};


	Vector3d position;
	Vector3d los;
	float psi;
	float theta;

	Vector2d mousePos;

	std::vector<bool> keys;
	std::vector<int> possibleKeys;

	const float SENSITIVITY = .005;
	const float MOVEMENT_SPEED = 1;
	float lastFrame = 0.0f;
};

