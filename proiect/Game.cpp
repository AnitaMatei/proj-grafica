#include "Game.h"

Game Game::instance;

Game::Game()
{
}


void Game::start(Vector2d windowSize, int argc, char** argv)
{
	initWindow(windowSize, argc, argv);

	initializeGame();
	createWalls();
	createBalls();
	camera = new Camera;
	glutDisplayFunc(drawGame);
	glutIdleFunc(updateGame);
	glutMouseFunc(checkMouseInput);
	glutPassiveMotionFunc(checkPassiveMouseMotion);
	glutMotionFunc(checkMouseMotion);
	glutKeyboardFunc(checkKeyboardInput);
	glutKeyboardUpFunc(checkKeyboardInputUp);
	createMenus();
	glutMainLoop();

}

void Game::initWindow(Vector2d windowSize, int argc, char** argv)
{
	glutInit(&argc, argv); // initializare GLUT
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(windowSize.x, windowSize.y);
	glutCreateWindow("Biliard");

	glClearColor(0.0, 1.0, 0.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0, windowSize.x, windowSize.y, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	if (!render2D) {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_FOG);
	}
}

void Game::createBalls()
{
	balls.clear();

	//introduce all holes
	for (int i = 0; i < Rules::HOLES_POSITIONS.size(); i++) {
		balls.push_back(new Ball(Color::Gray, 30, engine, Rules::HOLES_POSITIONS.at(i), Vector2d(0, 0), Vector2d(0, 0), false));
	}

	cueBall = new Ball(Color::White, 20, engine, Vector2d(Rules::CUE_BALL_START_POSITION.x, Rules::CUE_BALL_START_POSITION.y), Vector2d(0, 0), Vector2d(0, 0));

	for (int i = 0; i < Rules::PLAY_BALLS_START_POSITIONS.size(); i++) {
		balls.push_back(new Ball(Rules::PLAY_BALLS_COLORS.at(i), 20, engine, Rules::PLAY_BALLS_START_POSITIONS.at(i), Vector2d(0, 0), Vector2d(0, 0), true));
	}

	cue = new Cue(Color::Sienna, cueBall);
}

void Game::createWalls()
{
	walls.push_back(new Wall(Vector2d(600, 25), 1200, 50, engine));
	walls.push_back(new Wall(Vector2d(600, 575), 1200, 50, engine));
	walls.push_back(new Wall(Vector2d(25, 300), 50, 600, engine));
	walls.push_back(new Wall(Vector2d(1175, 300), 50, 600, engine));
}


void Game::registerPlayer()
{
	ofstream fout;
	fout.open("leaderboards.txt", fstream::app);
	auto endTime = high_resolution_clock::now();
	milliseconds time = duration_cast<milliseconds>(endTime - startTime);
	fout << playerName << " " << score << " " << time.count() / 1000 + offset << endl;
	fout.close();
}

void Game::initializeGame()
{
	cout << "What is your name?" << endl;
	//cin >> playerName;
	playerName = "Mah boi";
	score = 0;
	offset = 0;
	gameOver = false;
	startTime = high_resolution_clock::now();
}

void Game::endGame()
{
	gameOver = true;
	registerPlayer();
}

void Game::updateGame(void)
{
	if (instance.gameOver)
		return;
	instance.camera->update(.016f);
	instance.engine.updatePhysics(.016f);
	instance.cue->update(.016f);

	for (int i = 0; i < instance.balls.size(); i++) {
		Ball* ball = instance.balls.at(i);
		if (ball->isIntoHole() && ball->getBody().getId() == 16) {
			instance.endGame();
		}
		else if (ball->isIntoHole()) {
			instance.score++;
			instance.balls.erase(instance.balls.begin() + i);
			delete ball;
		}
	}
	if (instance.cueBall->isIntoHole())
		instance.resetCueBall();

	glutPostRedisplay();
}

void Game::drawGame(void)
{
	if (instance.render2D)
		instance.drawGame2D();
	else instance.drawGame3D();
}

void Game::checkMouseInput(int button, int state, int x, int y)
{
	instance.cue->checkInput(button, state, x, y);
}

void Game::checkKeyboardInput(unsigned char key, int x, int y)
{
	if (key == 'r')
		instance.switchRenderMode();
	instance.camera->checkInput(key, x, y);
}

void Game::checkKeyboardInputUp(unsigned char key, int x, int y)
{
	instance.camera->checkInputUp(key, x, y);
}

void Game::checkPassiveMouseMotion(int x, int y)
{
	if(!instance.render2D)
		instance.camera->updateMousePos(x, y);
}

void Game::checkMouseMotion(int x, int y)
{
	instance.cue->updateMousePos(x, y);
}

void Game::resetCueBall()
{
	//penalty of 20 seconds
	offset += 20.0;
	instance.cueBall->setIntoHole(false);
	instance.cueBall->getBody().setPosition(Vector2d(Rules::CUE_BALL_START_POSITION.x, Rules::CUE_BALL_START_POSITION.y));
	instance.cueBall->getBody().setVelocity(Vector2d(0, 0));
	instance.cueBall->getBody().setAcceleration(Vector2d(0, 0));
}

void Game::drawUi()
{
	auto endTime = high_resolution_clock::now();
	double currentTime = duration_cast<milliseconds>(endTime - startTime).count() / 1000.0 + offset;
	string uiString = "Score: " + to_string(score);
	uiString.append(" Time: ");
	uiString.append(to_string(currentTime));

	const unsigned char* t = reinterpret_cast<const unsigned char*>(uiString.c_str());
	glPushMatrix();
	glColor3f(Color::Black.r, Color::Black.g, Color::Black.b);
	glRasterPos2i(50, 50);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, t);

	if (gameOver)
	{
		t = reinterpret_cast<const unsigned char*>("GAME OVER");
		glRasterPos2i(550, 250);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, t);
	}
	glPopMatrix();

}

void Game::drawTable()
{
	glPushMatrix();

	float diffuseCoeff[] = { Color::Green.r * .95f, Color::Green.g * .95f, Color::Green.b * .95f,1.0f };
	float specularCoeff[] = { 1, 1, 1 ,1.0f };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diffuseCoeff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularCoeff);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 125.0);


	glBegin(GL_QUADS);
	glVertex3f(0.0f, Rules::TABLE_HEIGHT_3D, 0.0f);
	glVertex3f(1200.0f, Rules::TABLE_HEIGHT_3D, 0.0f);
	glVertex3f(1200.0f, Rules::TABLE_HEIGHT_3D, 600.0f);
	glVertex3f(0.0f, Rules::TABLE_HEIGHT_3D, 600.0f);
	glEnd();
	glPopMatrix();
}


void Game::switchRenderMode()
{
	if (!render2D) {
		instance.render2D = true;
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glDisable(GL_FOG);
		glutPostRedisplay();
	}
	else {
		instance.render2D = false;
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_FOG);
		glutPostRedisplay();
	}
}

void Game::shadowmatrix(float shadowMat[4][4], float groundplane[4], float lightpos[4])
{
	float dot;

		dot = groundplane[0] * lightpos[0] +
			groundplane[1] * lightpos[1] +
			groundplane[2] * lightpos[2] +
			groundplane[3] * lightpos[3];

		shadowMat[0][0] = dot - lightpos[0] * groundplane[0];
		shadowMat[1][0] = 0.f - lightpos[0] * groundplane[1];
		shadowMat[2][0] = 0.f - lightpos[0] * groundplane[2];
		shadowMat[3][0] = 0.f - lightpos[0] * groundplane[3];

		shadowMat[0][1] = 0.f - lightpos[1] * groundplane[0];
		shadowMat[1][1] = dot - lightpos[1] * groundplane[1];
		shadowMat[2][1] = 0.f - lightpos[1] * groundplane[2];
		shadowMat[3][1] = 0.f - lightpos[1] * groundplane[3];

		shadowMat[0][2] = 0.f - lightpos[2] * groundplane[0];
		shadowMat[1][2] = 0.f - lightpos[2] * groundplane[1];
		shadowMat[2][2] = dot - lightpos[2] * groundplane[2];
		shadowMat[3][2] = 0.f - lightpos[2] * groundplane[3];

		shadowMat[0][3] = 0.f - lightpos[3] * groundplane[0];
		shadowMat[1][3] = 0.f - lightpos[3] * groundplane[1];
		shadowMat[2][3] = 0.f - lightpos[3] * groundplane[2];
		shadowMat[3][3] = dot - lightpos[3] * groundplane[3];
}


void Game::createSubMenu(int id)
{
	switch (id) {
	case 1:
		if (!instance.render2D) {
			instance.switchRenderMode();
		}
		break;
	case 2:
		if (instance.render2D)
		{
			instance.switchRenderMode();
		}
		break;
	}
}

void Game::createMenus()
{
	int subMenu = glutCreateMenu(createSubMenu);
	glutAddMenuEntry("2D", 1);
	glutAddMenuEntry("3D", 2);
	glutCreateMenu(createMainMenu);
	glutAddSubMenu("RENDER MODE", 1);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void Game::createMainMenu(int id)
{
}

void Game::drawGame2D(void)
{
	prepareDrawGame2D();

	for (auto wall : instance.walls) {
		wall->draw();
	}
	for (auto ball : instance.balls) {
		ball->draw();
	}
	instance.cueBall->draw();
	instance.cue->draw();
	instance.drawUi();
	glutSwapBuffers();
	glFlush();
}

void Game::drawGame3D(void)
{
	prepareDrawGame3D();
	float floorshadow[4][4];
	float lightPosition[] = { 600, 100, 300, 1.0 };
	float plane[] = { 0,-720000,0,36000000};
	shadowmatrix(floorshadow, plane, lightPosition);


	gluLookAt(instance.camera->getPosition().x, instance.camera->getPosition().y, instance.camera->getPosition().z,
		instance.camera->getLos().x, instance.camera->getLos().y, instance.camera->getLos().z,
		0.0f, 1.0f, 0.0f);

	instance.drawTable();

	for (auto wall : instance.walls) {
		wall->draw3D();
	}
	for (auto ball : instance.balls) {
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glPushMatrix();
		glColor3f(0.f, 0.f, 0.f);  /* shadow color */
		glMultMatrixf((GLfloat *)floorshadow);
		ball->draw3D();
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		ball->draw3D();
	}
	instance.cueBall->draw3D();
	instance.cue->draw3D();

	float ambient[] = { 0.15f, 0.2f, 0.2f, 1.0f };
	float diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float specular[] = { .65f, .5f, .25f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	glEnable(GL_LIGHT0);

	float fogColor[4] = { .5, 0.5, 0.5, .1 };
	glFogi(GL_FOG_MODE, GL_EXP);
	glFogfv(GL_FOG_COLOR, fogColor);
	glFogf(GL_FOG_DENSITY, 0.0011);
	glFogf(GL_FOG_START, 1.0);
	glFogf(GL_FOG_END, 1.5);

	glutSwapBuffers();
	glFlush();



}

void Game::prepareDrawGame3D()
{
	int w = glutGet(GLUT_WINDOW_WIDTH);
	int h = glutGet(GLUT_WINDOW_HEIGHT);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	float ratio = w * 1.0 / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45.0f, ratio, 0.1f, 300000.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
	// Reset Matrix
	glLoadIdentity();
}

void Game::prepareDrawGame2D()
{

	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(0.0, 1.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// Set the viewport to be the entire window
	gluOrtho2D(0.0, 1200.0, 600.0, 0.0); // sunt indicate coordonatele extreme ale ferestrei de vizualizare
	glMatrixMode(GL_MODELVIEW);  // se precizeaza este vorba de o reprezentare 2D, realizata prin proiectie ortogonala
	glLoadIdentity();

}
