#include "PhysicsEngine.h"



PhysicsEngine::PhysicsEngine()
{
}


PhysicsEngine::~PhysicsEngine()
{
}

void PhysicsEngine::addBody(Body* body) {
	bodies.push_back(body);
}

void PhysicsEngine::updatePhysics(double deltaTime) {
	for (auto& body : bodies) {
		if (body->getType() != Body::Circle || !body->isActive())
			continue;

		CircleBody & cbody = dynamic_cast<CircleBody&>(*body);
		//simulate friction
		body->setAcceleration(Vector2d(body->getVelocity().x * -.95f, body->getVelocity().y * -.95f));

		//update velocity and position using the time since last calculation
		body->addToVelocity(Vector2d(body->getAcceleration().x * deltaTime, body->getAcceleration().y * deltaTime));
		body->addToPosition(Vector2d(body->getVelocity().x * deltaTime, body->getVelocity().y * deltaTime));

		//easy wall collision cause time is running out
		if (body->getPosition().x - cbody.getRadius() < 50)
			body->setVelocity(Vector2d(-body->getVelocity().x, body->getVelocity().y));
		if (body->getPosition().x + cbody.getRadius() > 1150)
			body->setVelocity(Vector2d(-body->getVelocity().x, body->getVelocity().y));
		if (body->getPosition().y - cbody.getRadius() < 50)
			body->setVelocity(Vector2d(body->getVelocity().x, -body->getVelocity().y));
		if (body->getPosition().y + cbody.getRadius() > 550)
			body->setVelocity(Vector2d(body->getVelocity().x, -body->getVelocity().y));

		//clamp the speed to 0
		if (pow(body->getVelocity().x, 2) + pow(body->getVelocity().y, 2) < 20)
			body->setVelocity(Vector2d(0, 0));
	}
	for (auto body : bodies) {
		for (auto target : bodies) {
			if (body->getId() == target->getId())
				continue;
			if (body->getType() == Body::Circle && target->getType() == Body::Circle) {
				CircleBody& cb1 = dynamic_cast<CircleBody&>(*body);
				CircleBody& cb2 = dynamic_cast<CircleBody&>(*target);
				int prevRad1 = cb1.getRadius(),
					prevRad2 = cb2.getRadius();
				//make the hitbox when colliding with holes smaller
				if (isHole(cb1))
					cb2.setRadius(15);
				if (isHole(cb2))
					cb1.setRadius(15);
				if (doCirclesCollide(cb1, cb2)) {
					body->hasCollided(target->getId());
					handleCollidingCircles(cb1, cb2);
				}
				cb1.setRadius(prevRad1);
				cb2.setRadius(prevRad2);
			}


		}
	}

	//since we don't wanna delete a body in the middle of calculating collisions, we just delete it after every object's physics are updated
	emptyBodyRemovalQueue();
}

void PhysicsEngine::addToBodyRemovalQueue(Body * body)
{
	bodyRemovalQueue.push_back(body);
}

void PhysicsEngine::handleCollidingCircles(CircleBody & b1, CircleBody & b2) {
	//displace both circles so that they dont sit on top of each other

	double distance = getDistanceBetweenCircles(b1, b2),
		overlap = .5f * (distance - b1.getRadius() - b2.getRadius());

	if (b1.isActive())
		b1.addToPosition(Vector2d(-overlap * (b1.getPosition().x - b2.getPosition().x) / distance,
			-overlap * (b1.getPosition().y - b2.getPosition().y) / distance));


	if (b2.isActive())
		b2.addToPosition(Vector2d(overlap * (b1.getPosition().x - b2.getPosition().x) / distance,
			overlap * (b1.getPosition().y - b2.getPosition().y) / distance));

	Vector2d normal((b2.getPosition().x - b1.getPosition().x) / distance,
		(b2.getPosition().y - b1.getPosition().y) / distance);

	Vector2d veld(b1.getVelocity().x - b2.getVelocity().x, b1.getVelocity().y - b2.getVelocity().y);
	float prodScalar = normal.x * veld.x + normal.y * veld.y;

	if(b1.isActive())
		b1.addToVelocity(Vector2d(-prodScalar * normal.x, - prodScalar * normal.y));
	if(b2.isActive())
		b2.addToVelocity(Vector2d(prodScalar * normal.x, prodScalar * normal.y));

}

void PhysicsEngine::emptyBodyRemovalQueue()
{
	while (bodyRemovalQueue.size() > 0) {
		Body* body = bodyRemovalQueue.back();
		for (int i = 0; i < Body::count; i++) {
			if (body->getId() == bodies.at(i)->getId())
			{
				bodies.erase(bodies.begin() + i);
				break;
			}
		}
		bodyRemovalQueue.pop_back();
	}
}

bool PhysicsEngine::isHole(CircleBody & b)
{
	for (int i = 0; i < Rules::HOLES_IDS.size(); i++) {
		if (b.getId() == Rules::HOLES_IDS.at(i))
			return true;
	}
	return false;
}

bool PhysicsEngine::doCirclesCollide(CircleBody & b1, CircleBody & b2) {
	return b1.getRadius() + b2.getRadius() > getDistanceBetweenCircles(b1, b2);
}

double PhysicsEngine::getDistanceBetweenCircles(Body & b1, Body & b2) {
	return sqrt(pow(b2.getPosition().x - b1.getPosition().x, 2) + pow(b2.getPosition().y - b1.getPosition().y, 2));
}
