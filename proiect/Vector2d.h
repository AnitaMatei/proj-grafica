#pragma once
#include <math.h>

class Vector2d
{
public:
	Vector2d(double x = 0, double y = 0);
	~Vector2d();

	Vector2d multiplyBy(double factor);

	static Vector2d normalise(Vector2d & a, Vector2d & b);
	double x, y;
};

