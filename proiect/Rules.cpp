#include "Rules.h"

vector<Vector2d> Rules::HOLES_POSITIONS = {
	Vector2d(45, 45),
Vector2d(570, 30),
Vector2d(1155, 45),
Vector2d(30,270),
Vector2d(1170,270),
Vector2d(45,555),
Vector2d(570, 570),
Vector2d(1155, 555)
};

vector<int> Rules::HOLES_IDS = {
	 0,1,2,3,4,5,6,7
};

Vector2d Rules::CUE_BALL_START_POSITION = Vector2d(600, 450);
int Rules::CUE_BALL_ID = 8;

vector<Vector2d> Rules::PLAY_BALLS_START_POSITIONS = {
	Vector2d(200,200), Vector2d(200,260),Vector2d(200,320),
	Vector2d(260, 200),Vector2d(260,260),Vector2d(260,320),
	Vector2d(320,200),Vector2d(320,260),Vector2d(320,320)
};

vector<int> Rules::PLAY_BALLS_IDS = {
	9,10,11,12,13,14,15,16,17
};

vector<Color> Rules::PLAY_BALLS_COLORS = {
	Color::Red,Color::SandyBrown,Color::Aqua,
	Color::Lime,Color::Blue,Color::Violet,
	Color::Gold,Color::Black,Color::Pink
};

double Rules::TABLE_HEIGHT_3D = 50.0;

Rules::Rules()
{
}


Rules::~Rules()
{
}
