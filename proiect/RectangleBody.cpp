#include "RectangleBody.h"



RectangleBody::RectangleBody()
{
}


RectangleBody::~RectangleBody()
{
}

RectangleBody::RectangleBody(Vector2d position, int width, int height, DrawableEntity* parent, bool active): 
	Body(position, Vector2d(0,0), Vector2d(0,0), 1, Body::Rectangle, parent, active)
{
	this->width = width;
	this->height = height;
}

void RectangleBody::hasCollided(int collidedWithId)
{
	parent->physicalBodyHasCollided(collidedWithId);
}

int RectangleBody::getWidth()
{
	return width;
}

int RectangleBody::getHeight()
{
	return height;
}
