#include "Ball.h"

const double Ball::TWO_PI = 6.2831853;

Ball::Ball()
{
}


Ball::~Ball()
{
}

void Ball::update(float deltaTime)
{
}

void Ball::draw() {
	glPushMatrix();
	glTranslated(body.getPosition().x, body.getPosition().y, 0);
	glCallList(displayList2D);
	glPopMatrix();
}

void Ball::draw3D()
{
	glPushMatrix();
	Vector3d position = Vector3d::translate2DTo3D(body.getPosition());
	position.y = Rules::TABLE_HEIGHT_3D;

	float diffuseCoeff[] = { color.r * .7f, color.g * .7f, color.b * .7f,1.0f };
	float specularCoeff[] = {1, 1, 1 ,1.0f };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diffuseCoeff);
	glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, specularCoeff);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 125.0);
	//if it isnt a hole we offset the ball by its radius on the y axis
	if (body.isActive())
		position.y += body.getRadius();
	glTranslated(position.x, position.y, position.z);

	glCallList(displayList3D);


	glPopMatrix();
}

void Ball::physicalBodyHasCollided(int collidedWithId)
{
	//if a ball collides with the holes
	if (collidedWithId >= Rules::HOLES_IDS.at(0) && collidedWithId <= Rules::HOLES_IDS.at(Rules::HOLES_IDS.size() - 1))
	{
		intoHole = true;

		//if its not the cue ball
		if (body.getId() != Rules::CUE_BALL_ID)
			engine->addToBodyRemovalQueue((Body*)& body);
	}
}

//projects a point from inside or outside of a circle onto its circumference
Vector2d Ball::projectPointOntoCircumference(Vector2d p)
{
	Vector2d norm = Vector2d::normalise(p, body.getPosition());
	Vector2d returnValue(norm.x * body.getRadius(), norm.y * body.getRadius());
	return returnValue;
}

CircleBody& Ball::getBody()
{
	return body;
}

Color& Ball::getColor()
{
	return color;
}

bool Ball::isIntoHole()
{
	return intoHole;
}

void Ball::setIntoHole(bool intoHole)
{
	this->intoHole = intoHole;
}

void Ball::create2DObject()
{
	Vector2d hexVertex;
	GLdouble hexTheta;
	GLint k;
	displayList2D = glGenLists(1);

	glNewList(displayList2D, GL_COMPILE);

	glColor3f(this->color.r, this->color.g, this->color.b);
	glBegin(GL_POLYGON);
	for (k = 0; k < 100; k++)
	{
		hexTheta = TWO_PI * k / 100;
		hexVertex.x = body.getRadius() * cos(hexTheta);
		hexVertex.y = body.getRadius() * sin(hexTheta);
		glVertex2i(hexVertex.x, hexVertex.y);
	}
	glEnd();

	glColor3f(Color::Black.r, Color::Black.g, Color::Black.b);
	glLineWidth(3);
	glBegin(GL_LINE_LOOP);
	for (k = 0; k < 100; k++)
	{
		hexTheta = TWO_PI * k / 100;
		hexVertex.x = body.getRadius() * cos(hexTheta);
		hexVertex.y = body.getRadius() * sin(hexTheta);
		glVertex2i(hexVertex.x, hexVertex.y);
	}
	glEnd();

	glColor3f(0, 0, 0);
	glBegin(GL_LINE);
	glVertex2i(body.getPosition().x, body.getPosition().y);
	glVertex2i(body.getVelocity().x, body.getVelocity().y);
	glEnd();

	glEndList();
}

void Ball::create3DObject()
{
	displayList3D = glGenLists(1);

	glNewList(displayList3D, GL_COMPILE);
	glColor3f(this->color.r, this->color.g, this->color.b);
	glutSolidSphere(body.getRadius(), 100, 10);

	glEndList();
}


Ball::Ball(Color color, int radius, PhysicsEngine & engine, Vector2d  const& position, Vector2d const& velocity, Vector2d  const& acceleration, bool active) : body(position, velocity, acceleration, 1, radius, this, active) {
	engine.addBody(&this->body);
	this->engine = &engine;
	this->color = color;
	this->intoHole = false;

	this->create2DObject();
	this->create3DObject();
}
