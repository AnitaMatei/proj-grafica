#include "Body.h"

int Body::count = 0;

Body::Body()
{
}


Body::~Body()
{
}

Vector2d & Body::getAcceleration() {
	return acceleration;
}

void Body::setAcceleration(Vector2d const &acceleration) {
	this->acceleration.x = acceleration.x;
	this->acceleration.y = acceleration.y;
}

void Body::setVelocity(Vector2d const &velocity) {
	this->velocity.x = velocity.x;
	this->velocity.y = velocity.y;
}

Vector2d & Body::getVelocity() {
	return velocity;
}

Vector2d & Body::getPosition() {
	return position;
}

void Body::setPosition(Vector2d const &position)
{
	this->position.x = position.x;
	this->position.y = position.y;
}

void Body::addToAcceleration(Vector2d const &addition) {
	this->acceleration.x = addition.x;
	this->acceleration.y = addition.y;
}

void Body::addToVelocity(Vector2d const&addition) {
	this->velocity.x += addition.x;
	this->velocity.y += addition.y;
}

void Body::addToPosition(Vector2d const&addition) {
	this->position.x += addition.x;
	this->position.y += addition.y;
}

double Body::getMass() {
	return mass;
}


Body::BodyType Body::getType() {
	return bodyType;
}

int Body::getId() {
	return id;
}

bool Body::isActive()
{
	return active;
}

void Body::activate(bool active)
{
	this->active = active;
}


Body::Body(Vector2d const&position, Vector2d const&velocity, Vector2d const&acceleration, double mass, BodyType bodyType, DrawableEntity* parent, bool active) {
	this->velocity.x = velocity.x;
	this->velocity.y = velocity.y;
	this->acceleration.x = acceleration.x;
	this->acceleration.y = acceleration.y;
	this->position.x = position.x;
	this->position.y = position.y;
	this->mass = mass;
	this->bodyType = bodyType;
	this->id = count;
	this->parent = parent;
	this->active = active;
	Body::count++;

	active = true;
}
