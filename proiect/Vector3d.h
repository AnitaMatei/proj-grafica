#pragma once
#include "Vector2d.h"
class Vector3d :
	public Vector2d
{
public:
	Vector3d(double x = 0, double y = 0, double z = 0);
	~Vector3d();

	Vector3d multiplyBy(double factor);

	static Vector3d normalise(Vector3d & a, Vector3d & b);
	static Vector3d translate2DTo3D(Vector2d &a);

	double z;
};

