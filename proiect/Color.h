#pragma once
class Color
{
public:
	Color();
	Color(float r, float g, float b);
	~Color();
	
	static Color Red, Green, Blue,
		Pink, Violet, Orange, Yellow, Gold,
		Lime, SpringGreen, Black, White, Gray, Aqua,
		SandyBrown, Brown, Sienna, LawnGreen;

	float r;
	float g;
	float b;
};

