#include "Cue.h"


const double Cue::HIT_ANIMATION_LENGTH = .5;


Cue::Cue()
{
}

Cue::Cue(Color color, Ball* cueBall) : mousePos(0, 0), hitAnimationVector(0, 0), hitStrength(0.0)
{
	this->color = color;
	this->cueBall = cueBall;
	definingPoints.push_back(Vector2d(-5, 150));
	definingPoints.push_back(Vector2d(5, 150));
	definingPoints.push_back(Vector2d(5, -150));
	definingPoints.push_back(Vector2d(-5, -150));

	create2DObject();
	create3DObject();
}


Cue::~Cue()
{
}

void Cue::update(float deltaTime)
{
	if (released)
	{
		//the progress vector receives a percentage of the animation vector
		//the percentage is calculated by dividing how much time passed by how long the animation takes
		hitAnimationProgress.x = hitAnimationVector.x * (hitAnimationTimer / HIT_ANIMATION_LENGTH);
		hitAnimationProgress.y = hitAnimationVector.y * (hitAnimationTimer / HIT_ANIMATION_LENGTH);
		hitAnimationTimer += deltaTime;

		//if the timer reached its limit, reset the timer and hit the ball
		if (hitAnimationTimer > HIT_ANIMATION_LENGTH) {
			Vector2d dir = getMouseBallNormalised();
			Vector2d velocity(dir.x * -hitStrength * 10, dir.y * -hitStrength * 10);
			cueBall->getBody().setVelocity(velocity);
			released = false;
			hitAnimationTimer = 0;
		}
	}
	if (!held)
		return;

	//calculate how hard to hit the cue ball depending on how far the cue is
	Vector2d ballMouse = getMouseBall();
	hitStrength = sqrt(pow(ballMouse.x, 2) + pow(ballMouse.y, 2));

}



void Cue::draw()
{
	if (!held && !released)
		return;
	Vector2d mouseBall = getMouseBall();
	double rotation = -atan2(mouseBall.x, mouseBall.y);


	rotation = rotation * 180 / 3.14;
	cout << rotation << endl;
	//first, the cue is rotated when its in the origin
	//then, the cue is moved to the position of the ball
	//then, the cue is offset by the vector between the mouse and the ball, up to a certain limit
	//then, if the animation started, offset the cue corresponding to the animation progress
	glPushMatrix();
	if (released)
		glTranslated(hitAnimationProgress.x, hitAnimationProgress.y, 0);
	glTranslated(mouseBall.x, mouseBall.y, 0);
	glTranslated(cueBall->getBody().getPosition().x, cueBall->getBody().getPosition().y, 0);
	glRotated(rotation, 0, 0, 1);
	glCallList(displayList2D);
	glEnd();
	glPopMatrix();
}

void Cue::draw3D()
{
	if (!held && !released)
		return;
	Vector2d mouseBall = getMouseBall();
	double rotation = atan2(mouseBall.x, mouseBall.y);

	double positionY = Rules::TABLE_HEIGHT_3D + 20;

	Vector3d hitAnimationProgress3D = Vector3d::translate2DTo3D(hitAnimationProgress);
	Vector3d mouseBall3D = Vector3d::translate2DTo3D(mouseBall);
	Vector3d cueBallPos3D = Vector3d::translate2DTo3D(cueBall->getBody().getPosition());
	cueBallPos3D.y = positionY;


	rotation = rotation * 180 / 3.14;


	glPushMatrix();

	float diffuseCoeff[] = { color.r * .3f, color.g * .3f, color.b * .3f,1.0f };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diffuseCoeff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, diffuseCoeff);

	//first, the cue is rotated when its in the origin
	//then, the cue is moved to the position of the ball
	//then, the cue is offset by the vector between the mouse and the ball, up to a certain limit
	//then, if the animation started, offset the cue corresponding to the animation progress
	if (released)
		glTranslated(hitAnimationProgress3D.x, hitAnimationProgress3D.y, hitAnimationProgress3D.z);
	glTranslated(mouseBall3D.x, mouseBall3D.y, mouseBall3D.z);
	glTranslated(cueBallPos3D.x, cueBallPos3D.y, cueBallPos3D.z);
	glRotated(rotation, 0, 1, 0);
	glCallList(displayList3D);
	glEnd();
	glPopMatrix();
}

void Cue::checkInput(int button, int state, int x, int y)
{
	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN && cueBall->getBody().getVelocity().x == 0 && cueBall->getBody().getVelocity().y == 0)
		{
			held = true;
		}
		else if (state == GLUT_UP) {
			//when the mouse is released calculate the animation vector on which the cue will move
			if (held) {
				held = false;
				released = true;
				Vector2d mouseBall = getMouseBall();
				Vector2d circumProjection = cueBall->projectPointOntoCircumference(Vector2d(x, y));
				hitAnimationVector = Vector2d(circumProjection.x - mouseBall.x, circumProjection.y - mouseBall.y);
			}
		}
		break;
	default:
		break;
	}
}

void Cue::updateMousePos(int x, int y)
{
	mousePos.x = x;
	mousePos.y = y;
}

void Cue::create2DObject()
{
	displayList2D = glGenLists(1);

	glNewList(displayList2D, GL_COMPILE);

	glColor3f(this->color.r, this->color.g, this->color.b);
	glBegin(GL_POLYGON);
	glVertex2i(-5, 300);
	glVertex2i(5, 300);
	glVertex2i(5, 0);
	glVertex2i(-5, 0);
	glEnd();

	glEndList();
}

void Cue::create3DObject()
{
	displayList3D = glGenLists(1);

	glNewList(displayList3D, GL_COMPILE);
	//height relative to the table
	float positionY = 5;

	glBegin(GL_QUADS);
	// Top face CCW
	glColor3f(color.r, color.g, color.b);
	glVertex3f(-5, positionY, 300);
	glVertex3f(-5, positionY, 0);
	glVertex3f(5, positionY, 0);
	glVertex3f(5, positionY, 300);


	//// Bottom face
	glColor3f(color.r, color.g, color.b);
	glVertex3f(-5, -positionY, 300);
	glVertex3f(-5, -positionY, 0);
	glVertex3f(5, -positionY, 0);
	glVertex3f(5, -positionY, 300);

	//// Front face
	glVertex3f(-5, positionY, 0);
	glVertex3f(-5, -positionY, 0);
	glVertex3f(5, -positionY, 0);
	glVertex3f(5, positionY, 0);

	//// Back face
	glVertex3f(-5, positionY, 300);
	glVertex3f(-5, -positionY, 300);
	glVertex3f(5, -positionY, 300);
	glVertex3f(5, positionY, 300);

	//// Left face
	glVertex3f(-5, positionY, 300);
	glVertex3f(-5, -positionY, 300);
	glVertex3f(-5, -positionY, 0);
	glVertex3f(-5, positionY, 0);

	//// Right face
	glVertex3f(5, positionY, 0);
	glVertex3f(5, -positionY, 0);
	glVertex3f(5, -positionY, 300);
	glVertex3f(5, positionY, 300);



	glEnd();

	glEndList();
}

void Cue::physicalBodyHasCollided(int collidedWithId)
{
}

Vector2d Cue::getMouseBallNormalised()
{
	return Vector2d::normalise(mousePos, cueBall->getBody().getPosition());
}

//gets the vector from the ball to the mouse, limited by the maximum offset value
Vector2d Cue::getMouseBall()
{
	Vector2d mouseBall(mousePos.x - cueBall->getBody().getPosition().x,
		mousePos.y - cueBall->getBody().getPosition().y);
	double mouseBallMagnitude = sqrt(pow(mouseBall.x, 2) + pow(mouseBall.y, 2));

	if (mouseBallMagnitude >= MAX_OFFSET) {
		Vector2d normMouseBall = getMouseBallNormalised();
		mouseBall.x = MAX_OFFSET * normMouseBall.x;
		mouseBall.y = MAX_OFFSET * normMouseBall.y;
	}
	if (mouseBallMagnitude < cueBall->getBody().getRadius()) {
		mouseBall = cueBall->projectPointOntoCircumference(mouseBall);
	}


	return mouseBall;
}
