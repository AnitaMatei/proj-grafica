#include "Camera.h"



Camera::Camera() : position(640, 980, 700.5), los(640, 980, 700),
psi(2.5), theta(3.1), mousePos(0, 0), possibleKeys(5,0), keys(255,false)
{
	
	possibleKeys[0] = FORWARD;
	possibleKeys[1] = BACKWARDS;
	possibleKeys[2] = LEFT;
	possibleKeys[3] = RIGHT;
	possibleKeys[4] = STOP_CAMERA;
}


void Camera::update(float deltaTime)
{
	if (keys['q'])
		return;
	float time = glutGet(GLUT_ELAPSED_TIME) - lastFrame;
	lastFrame = glutGet(GLUT_ELAPSED_TIME);
	if (keys[FORWARD]) {
		position.x += sin(theta) * sin(psi) * MOVEMENT_SPEED * time;
		position.y += cos(psi) * MOVEMENT_SPEED * time;
		position.z += cos(theta) * sin(psi) * MOVEMENT_SPEED * time;
	}
	if (keys[BACKWARDS]) {
		position.x -= sin(theta) * sin(psi) * MOVEMENT_SPEED * time;
		position.y -= cos(psi) * MOVEMENT_SPEED * time;
		position.z -= cos(theta) * sin(psi) * MOVEMENT_SPEED * time;
	}
	if (keys[LEFT]) {
		position.x -= sin(theta - 3.14 / 2) * sin(psi) * MOVEMENT_SPEED * time;
		position.z -= cos(theta - 3.14 / 2) * sin(psi) * MOVEMENT_SPEED * time;
	}
	if (keys[RIGHT]) {
		position.x -= sin(theta + 3.14 / 2) * sin(psi) * MOVEMENT_SPEED * time;
		position.z -= cos(theta + 3.14 / 2) * sin(psi) * MOVEMENT_SPEED * time;
	}


	los.x = position.x + sin(theta) * sin(psi);
	los.y = position.y + cos(psi);
	los.z = position.z + cos(theta) * sin(psi);
}


void Camera::checkMouseInput(int button, int state, int x, int y)
{

}

void Camera::checkInput(unsigned char key, int x, int y)
{
	for (int i = 0; i < possibleKeys.size(); i++)
		if (key == possibleKeys[i])
			keys[possibleKeys[i]] = true;
}

void Camera::checkInputUp(unsigned char key, int x, int y)
{
	for (int i = 0; i < possibleKeys.size(); i++)
		if (key == possibleKeys[i])
			keys[possibleKeys[i]] = false;
}

void Camera::updateMousePos(int x, int y)
{
	if (keys[STOP_CAMERA])
		return;
	if (abs(x - mousePos.x) > 200 || abs(y - mousePos.y) > 200)
	{
		mousePos = Vector2d(x, y);
		return;
	}
	theta -= (x - mousePos.x) * SENSITIVITY;
	psi += (y - mousePos.y) * SENSITIVITY;
	if (psi <= 0.05)
		psi = 0.05;
	else if (psi >= 0.95 * 3.14)
		psi = 0.95 * 3.14;
	mousePos = Vector2d(x, y);
}


const Vector3d & Camera::getPosition()
{
	return position;
}

const Vector3d& Camera::getLos()
{
	return los;
}

Camera::~Camera()
{
}
