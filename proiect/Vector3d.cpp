#include "Vector3d.h"




Vector3d::Vector3d(double x, double y, double z) : Vector2d(x, y)
{
	this->z = z;
}

Vector3d::~Vector3d()
{
}

Vector3d Vector3d::multiplyBy(double factor)
{
	return Vector3d(x*factor,y*factor,z*factor);
}

Vector3d Vector3d::normalise(Vector3d & a, Vector3d & b)
{
	double magnitude = sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2) + pow(a.z - b.z, 2));
	Vector3d returnVal((a.x - b.x) / magnitude, (a.y - b.y) / magnitude, (a.z - b.z) / magnitude);
	return returnVal;
}

Vector3d Vector3d::translate2DTo3D(Vector2d & a)
{
	Vector3d newVec(a.x, 0, a.y);
	return newVec;
}
